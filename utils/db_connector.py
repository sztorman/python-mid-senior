import motor.motor_tornado


class DBConnector(object):

        def __init__(self, host, port, name):
            self.host = host
            self.port = port
            self.name = name
            self.dbconn = None
            self.client = None

        def _create_connection(self):
            return motor.motor_tornado.MotorClient(self.host, self.port)[self.name]

        def get_client(self):
            self.dbconn = self._create_connection()
            return self.dbconn

        def __exit__(self, exc_type, exc_val, exc_tb):
            self.dbconn.close()
