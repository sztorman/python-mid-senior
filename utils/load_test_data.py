import sys
from tornado.ioloop import IOLoop

sys.path.insert(0, "../")
from data.users import main_user
from data.links_initial import link1, link2, link3
from settings.tests import DATABASE
from utils.db_connector import DBConnector

db_connection = DBConnector(**DATABASE).get_client()
links = [link1, link2, link3]

i = 0


def add_links(result, error):
    global i
    if error:
        raise error
    i += 1
    if i < 3:
        db_connection.link.insert_one(links[i], callback=add_links)
    else:
        print("Created links")
        IOLoop.current().stop()


def my_callback(result, error):
    print("Create user")
    IOLoop.current().stop()


def delete_callback(result, error):
    print("Deleted")
    IOLoop.current().stop()


def remove_from_database():
    db_connection.user.remove({}, callback=delete_callback)
    IOLoop.current().start()
    db_connection.link.remove({}, callback=delete_callback)
    IOLoop.current().start()


def add_user():
    db_connection.user.insert_one(main_user, callback=my_callback)
    IOLoop.current().start()


def add_links_base():
    db_connection.link.insert_one(links[i], callback=add_links)
    IOLoop.current().start()


def main():
    remove_from_database()
    add_user()
    add_links_base()


if __name__ == '__main__':
    main()
