from handlers import LoginHandler, LinksHandler, LogoutHandler, SingleLinkHandler, PutDeleteUserHandler

ROUTES = [
    (r"/?", LinksHandler),
    (r"/link", SingleLinkHandler),
    (r"/link/(.*)", PutDeleteUserHandler),
    (r"/links", LinksHandler),
    (r"/auth/login", LoginHandler),
    (r"/auth/logout", LogoutHandler),
]
