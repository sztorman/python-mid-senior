SETTINGS = {
    "cookie_secret": "dsfsadfadsfadsfsadfdsafadsf",
    "login_url": "auth/login",
    'SHORTENER_PREFIX': 'http://localhost/',
    'SESSION_TIMEOUT': 0.01  # in part of day
}

PORT = 8888

SALT = '$2b$12$RwjjtCw/Px.MhHgjebjDs.'
SECRET = "secret"

SHORTENER_PREFIX = 'http://localhost/'

DATABASE = {
    'host': 'localhost',
    'port': 27017,
    'name': 'test_db',
}
