import json


class JsonSerializable(object):
    def toDict(self):
        return self.__dict__

    def toJson(self):
        return json.dumps(self.__dict__)

    def __repr__(self):
        return self.toDict()


class Link(JsonSerializable):
    def __init__(self, **link_dict):
        self.link = link_dict['link']
        self.short_link = link_dict['link']
        self.user = link_dict['user']
        self.is_public = link_dict['is_public']
        self.date_created = link_dict['date_created']
        self.expiry_delay = link_dict['expiry_delay']
        self.date_expiry = link_dict['date_expiry']
        self.id = link_dict['_id']

