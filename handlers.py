import datetime
import json
import uuid

import bcrypt as bcrypt
import tornado.escape
import tornado.web
from bson import json_util
from tornado import gen

from models import Link
from utils import decorators


class BaseHandler(tornado.web.RequestHandler):
    def get_login_url(self):
        pass

    def get_current_user(self):
        user_json = self.get_secure_cookie("user", max_age_days=self.settings['SESSION_TIMEOUT'])
        if user_json:
            return tornado.escape.json_decode(user_json)
        else:
            return None

    def set_current_user(self, user):
        if user:
            self.set_secure_cookie("user", tornado.escape.json_encode(user),
                                   expires_days=self.settings['SESSION_TIMEOUT'])  # logout user after 15 minutes of inactivity
        else:
            self.clear_cookie("user")

    def _write_404(self):
        result = {"status_code": 404, "reason": "method not exit at all...=("}
        self.write(tornado.escape.json_encode(result))
        self.finish()
        return

    def get(self):
        self._write_404()


class LinksHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        if self.get_current_user():
            cursor = self.settings['db'].link.find({'user': {'$eq': self.get_current_user()}})
        else:  # tutaj dać ora
            cursor = self.settings['db'].link.find(
                {'is_public': {'$eq': 'True'}, 'date_expiry': {'$gt': datetime.datetime.today(), '$ne': ['null', ""]}})
        links_list = yield cursor.to_list(10)
        links = [Link(**link).__dict__ for link in links_list]
        self.write(json_util.dumps(links))
        self.finish()


class SingleLinkHandler(BaseHandler):
    def _create_short_url(self, url):
        suffix = str(uuid.uuid4())[:8]
        return self.settings['SHORTENER_PREFIX'] + suffix

    @decorators.authenticated
    @gen.coroutine
    def post(self):
        link_data = json.loads(self.request.body)
        link_data['date_created'] = datetime.datetime.now()
        link_data['short_link'] = self._create_short_url(link_data['link'])
        # date_expiry is redundant, helper variable for easy searching in database, add null
        link_data['date_expiry'] = datetime.datetime.now() + datetime.timedelta(hours=int(link_data['expiry_delay']))
        result = yield self.settings['db'].link.insert_one(link_data)
        self.set_status(201)
        self.write(json_util.dumps({"message": "Data inserted", "id": result.inserted_id}))


class PutDeleteUserHandler(BaseHandler):

    @decorators.authenticated
    @gen.coroutine
    def delete(self, id):
        result = yield self.settings['db'].link.delete_one({'_id': int(id), 'user': self.get_current_user()})
        if result.deleted_count == 1:
            self.write("Data removed")
        else:
            self.set_status(500)

    @decorators.authenticated
    @gen.coroutine
    def put(self, id):
        link_data = json.loads(self.request.body)
        if 'expiry_delay' in link_data:
            link_data['date_expiry'] = datetime.datetime.now() + datetime.timedelta(
                hours=int(link_data['expiry_delay']))
        result = yield self.settings['db'].link.update_one({'_id': int(id)}, {'$set': link_data})
        if result.modified_count == 1:
            self.set_status(200)
            # return data and check
            self.write("Data updated")
        else:
            self.set_status(400)
            self.write("Error")


class LoginHandler(BaseHandler):
    @gen.coroutine
    def post(self):
        data = json.loads(self.request.body)
        username = data.get("username", "")
        password = data.get("password", "")
        if username and password:
            user = yield self.settings['db'].user.find_one({'username': username})
            hashed_pass = bcrypt.hashpw(password.encode('UTF-8'), user['password'].encode('UTF-8'))
            if user and user['password'] and hashed_pass.decode("utf-8") == user['password']:
                self.set_current_user(user['username'])
                self.write("User logged in")
                self.finish()
            else:
                self.set_status(401)
                self.write("Wrong username or password")
                self.finish()
        else:
            self.set_status(401)
            self.write("Wrong username or password")
            self.finish()


class LogoutHandler(BaseHandler):
    def get(self):
        self.set_status(200)
        if self.get_current_user():
            self.clear_cookie("user")
            self.write("Logged out")
        else:
            self.write("You werent logged in!")
