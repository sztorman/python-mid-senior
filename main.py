import argparse

from tornado.ioloop import IOLoop

from app import Application
from settings.dev import DevConfig
from settings.prod import ProdConfig


def main(config):
    application = Application(config)
    application.listen(config.PORT)
    IOLoop.instance().start()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--settings")
    args = parser.parse_args()
    config = DevConfig
    if args.settings == 'prod':
        config = ProdConfig
    main(config)
