import datetime

link1 = {
    "_id": 1,
    "name": "link1",
    "link": "localhost/t1",
    "short_link": "localhost/t1",
    "user": "test",
    "is_public": "True",
    "expiry_delay": "72",
    "date_created": datetime.datetime.now(),
    "date_expiry":  datetime.datetime.now() + datetime.timedelta(days=60)
}

link2 = {
    "name": "link2",
    "link": "localhost/t2",
    "short_link": "localhost/t2",
    "user": "test",
    "is_public": "False",
    "expiry_delay": "72",
    "date_created": datetime.datetime.now(),
    "date_expiry": datetime.datetime.now() + datetime.timedelta(days=-60)
}

link3 = {
    "name": "link3",
    "link": "localhost/t3",
    "short_link": "localhost/t3",
    "user": "",
    "is_public": "True",
    "expiry_delay": "",
    "date_created": datetime.datetime.now(),
    "date_expiry": ""
}

link4 = {
    "_id": 4,
    "name": "link1",
    "link": "localhost/t1",
    "short_link": "localhost/t1",
    "user": "test",
    "is_public": "True",
    "expiry_delay": "72",
    "date_created": datetime.datetime.now(),
    "date_expiry": datetime.datetime.now() + datetime.timedelta(days=-60)
}