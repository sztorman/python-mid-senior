# Tornado example

## Requirements

Python 3.6

MongoDB 3.6.3
and files from requirements folder

## Running server

python main.py --settings=[dev, prod]

Change this settings to your own one

## Running tests

* Change tests.py settings in settings dir
* Load data using script load_test_data.py (utils dir)
* Run tests from tests catalogue

## Other comments
* Use coroutines from tornado (but also can use coroutines from asyncio instead)
* In links mechanism used redundant date_expiry to check if link has expired. Other possibility - use async method to check if link has expired and set active flag to false.