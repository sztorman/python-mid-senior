# scenario - login/add url/edit url/delete url/logout
import json
import unittest

import tornado.web
from unittest import mock
from tornado.testing import AsyncHTTPTestCase

from data.links_initial import link4
from routes import ROUTES
from settings.tests import DATABASE, SETTINGS
from utils.db_connector import DBConnector


class TestLink(AsyncHTTPTestCase):
    def get_app(self):
        self.db_connection = DBConnector(**DATABASE).get_client()
        return tornado.web.Application(ROUTES, db=self.db_connection, **SETTINGS)

    def test_user_scenario(self):
        post_args = {'username': 'test', 'password': 'test'}
        response = self.fetch('/auth/login', method='POST', body=json.dumps(post_args))
        self.assertEqual(200, response.code)
        self.assertEqual(b'User logged in', response.body)
        # need to mock user because secret coockie doesnt work during tests, but it shouldnt be here
        with mock.patch('handlers.BaseHandler.get_current_user') as current_user:
            current_user.return_value = "test"
            link_new_data = link4.copy()
            del link_new_data['date_expiry']
            del link_new_data['date_created']
            link_new_data[
                "_id"] = 10  # use normalize id instead of automatic ones - easy way to passed through url arguments
            response_insert = self.fetch('/link', method='POST', body=json.dumps(link_new_data))
            response_dict = json.loads(response_insert.body)
            self.assertEqual(201, response_insert.code)
            self.assertEqual('Data inserted', response_dict['message'])
            self.assertEqual(10, response_dict['id'])
            link_new_data_put = {"is_public": "True", "expiry_delay": "72"}
            response_put = self.fetch('/link/4', method='PUT', body=json.dumps(link_new_data_put))
            self.assertEqual(200, response_put.code)
            self.assertEqual(b'Data updated', response_put.body)
            response_delete = self.fetch('/link/10', method='DELETE')
            self.assertEqual(200, response_delete.code)
            self.assertEqual(b'Data removed', response_delete.body)


if __name__ == '__main__':
    unittest.main()
