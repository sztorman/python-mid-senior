import json
import sys
import unittest
from unittest import mock

import tornado.web
from tornado.ioloop import IOLoop
from tornado.testing import AsyncHTTPTestCase

from data.links_initial import link1, link4
from settings.tests import DATABASE, SETTINGS
from utils.db_connector import DBConnector

sys.path.insert(0, "../")
from routes import ROUTES


class TestAuthApp(AsyncHTTPTestCase):
    def get_app(self):
        self.db_connection = DBConnector(**DATABASE).get_client()
        return tornado.web.Application(ROUTES, db=self.db_connection, **SETTINGS)

    def test_login_wrong_credentials(self):
        post_args = {'username': 'test1234'}
        response = self.fetch('/auth/login', method='POST', body=json.dumps(post_args))
        self.assertEqual(401, response.code)
        self.assertEqual(b'Wrong username or password', response.body)

    def test_get_non_exist_url(self):
        response = self.fetch('/statistics', method='GET')
        self.assertEqual(404, response.code)

    def test_login_right_credentials(self):
        post_args = {'username': 'test', 'password': 'test'}
        response = self.fetch('/auth/login', method='POST', body=json.dumps(post_args))
        self.assertEqual(200, response.code)
        self.assertEqual(b'User logged in', response.body)

    def test_logout_without_login(self):
        response = self.fetch('/auth/logout', method='GET')
        self.assertEqual(200, response.code)
        self.assertEqual(b'You werent logged in!', response.body)

    @mock.patch('handlers.BaseHandler.get_current_user')
    def test_logout_with_login(self, current_user):
        current_user.return_value = "test"
        response = self.fetch('/auth/logout', method='GET')
        self.assertEqual(200, response.code)
        self.assertEqual(b'Logged out', response.body)


class TestGetLinks(AsyncHTTPTestCase):
    def get_new_ioloop(self):
        return IOLoop.instance()

    def get_app(self):
        self.db_connection = DBConnector(**DATABASE).get_client()
        return tornado.web.Application(ROUTES, db=self.db_connection, **SETTINGS)

    def test_get_public_links(self):
        response = self.fetch('/links', method='GET')
        self.assertEqual(200, response.code)
        self.assertEqual(1, len(json.loads(response.body)))

    @mock.patch('handlers.BaseHandler.get_current_user')
    def test_get_private_links(self, current_user):
        current_user.return_value = "test"
        response = self.fetch('/links', method='GET')
        self.assertEqual(200, response.code)
        self.assertEqual(2, len(json.loads(response.body)))


class TestLink(AsyncHTTPTestCase):
    def get_app(self):
        self.db_connection = DBConnector(**DATABASE).get_client()
        return tornado.web.Application(ROUTES, db=self.db_connection, **SETTINGS)

    @mock.patch('handlers.BaseHandler.get_current_user')
    def test_insert_new_link(self, current_user):
        current_user.return_value = "test"
        link_new_data = link4.copy()
        del link_new_data['date_expiry']
        del link_new_data['date_created']
        response = self.fetch('/link', method='POST', body=json.dumps(link_new_data))
        response_dict = json.loads(response.body)
        self.assertEqual(201, response.code)
        self.assertEqual('Data inserted', response_dict['message'])
        self.assertEqual(4, response_dict['id'])

    @mock.patch('handlers.BaseHandler.get_current_user')
    def test_update_link(self, current_user):
        current_user.return_value = "test"
        link_new_data = {"is_public": "True", "expiry_delay": "72"}
        response = self.fetch('/link/4', method='PUT', body=json.dumps(link_new_data))
        self.assertEqual(200, response.code)
        self.assertEqual(b'Data updated', response.body)

    @mock.patch('handlers.BaseHandler.get_current_user')
    def test_update_link_error(self, current_user):
        current_user.return_value = "test"
        link_new_data = {"is_public": "True", "expiry_delay": "72"}
        response = self.fetch('/link/2', method='PUT', body=json.dumps(link_new_data))
        self.assertEqual(400, response.code)
        self.assertEqual(b'Error', response.body)

    @mock.patch('handlers.BaseHandler.get_current_user')
    def test_delete_link(self, current_user):
        current_user.return_value = "test"
        response = self.fetch('/link/1', method='DELETE')
        self.assertEqual(200, response.code)
        self.assertEqual(b'Data removed', response.body)


if __name__ == '__main__':
    unittest.main()
