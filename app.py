import tornado.web

from routes import ROUTES
from utils.db_connector import DBConnector


class Application(tornado.web.Application):
    def __init__(self, config):
        handlers = ROUTES

        settings = config.MAIN_SETTINGS

        db_connection = DBConnector(**config.DATABASE).get_client()

        tornado.web.Application.__init__(self, handlers, **settings, db=db_connection)
